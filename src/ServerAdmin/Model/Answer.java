package ServerAdmin.Model;

import Server.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Answer {
    public int answerID;
    public String description;
    public int answerAmount;
    public static ArrayList<Answer> printAllAnswers(int questionNo){
        ArrayList<Answer> answerList = new ArrayList<Answer>();
        try {
            //get Surveys
            String stmt = "SELECT * FROM pytania_odpowiedzi po LEFT JOIN odpowiedzi o ON po.id_odpowiedzi = o.id_odpowiedzi WHERE id_pytania = " + questionNo;
            ResultSet rsAnswer = DBUtil.dbExecuteQuery(stmt);
            Answer newAnswer = new Answer();
            System.out.println("ID\tDescription");

            while (rsAnswer.next()) {
                newAnswer.answerID = rsAnswer.getInt("id_odpowiedzi");
                newAnswer.description = rsAnswer.getString("odpowiedz");
                newAnswer.answerAmount = rsAnswer.getInt("ilosc_odpowiedzi");
                System.out.println(newAnswer.answerID + "\t|\t" + newAnswer.description);
                answerList.add(newAnswer);
                newAnswer = new Answer();
            }
            if(answerList.size() == 0)
                System.out.println("To pytanie nie zawiera żadnych odpowiedzi");
        }
        catch (SQLException e){
            System.out.println("SQL Error in printAllSurveys");
        }

        return answerList;
    }
    public static ArrayList<Answer> getAllAnswers(int questionNo){
        ArrayList<Answer> answerList = new ArrayList<Answer>();
        try {
            //get Surveys
            String stmt = "SELECT * FROM pytania_odpowiedzi po LEFT JOIN odpowiedzi o ON po.id_odpowiedzi = o.id_odpowiedzi WHERE id_pytania = " + questionNo;
            ResultSet rsAnswer = DBUtil.dbExecuteQuery(stmt);
            Answer newAnswer = new Answer();

            while (rsAnswer.next()) {
                newAnswer.answerID = rsAnswer.getInt("id_odpowiedzi");
                newAnswer.description = rsAnswer.getString("odpowiedz");
                newAnswer.answerAmount = rsAnswer.getInt("ilosc_odpowiedzi");
                answerList.add(newAnswer);
                newAnswer = new Answer();
            }
            if(answerList.size() == 0)
                System.out.println("To pytanie nie zawiera żadnych odpowiedzi");
        }
        catch (SQLException e){
            System.out.println("SQL Error in printAllSurveys");
        }

        return answerList;
    }
}
