package ServerAdmin.Model;

import Server.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Question {
    public int questionID;
    public String description;


    public static ArrayList<Question> printAllQuestions(int surveyNo){
        ArrayList<Question> questionList = new ArrayList<Question>();
        try {
            //get Surveys
            String stmt = "SELECT * FROM ankiety_pytania ap LEFT JOIN pytania p on ap.id_pytania = p.id_pytania WHERE id_ankiety = " + surveyNo;
            ResultSet rsQuestion = DBUtil.dbExecuteQuery(stmt);
            Question newQuestion = new Question();
            System.out.println("ID\tDescription");

            while (rsQuestion.next()) {
                newQuestion.questionID = rsQuestion.getInt("id_pytania");
                newQuestion.description = rsQuestion.getString("tresc");
                System.out.println(newQuestion.questionID + "\t|\t" + newQuestion.description);
                questionList.add(newQuestion);
                newQuestion = new Question();
            }
            if(questionList.size() == 0)
                System.out.println("Ta ankieta nie zawiera pytań");
        }
        catch (SQLException e){
            System.out.println("SQL Error in printAllSurveys");
        }

        return questionList;
    }

    public static ArrayList<Question> getAllQuestions(int surveyNo){
        ArrayList<Question> questionList = new ArrayList<Question>();
        try {
            //get Surveys
            String stmt = "SELECT * FROM ankiety_pytania ap LEFT JOIN pytania p on ap.id_pytania = p.id_pytania WHERE id_ankiety = " + surveyNo;
            ResultSet rsQuestion = DBUtil.dbExecuteQuery(stmt);
            Question newQuestion = new Question();
            while (rsQuestion.next()) {
                newQuestion.questionID = rsQuestion.getInt("id_pytania");
                newQuestion.description = rsQuestion.getString("tresc");
                questionList.add(newQuestion);
                newQuestion = new Question();
            }
            if(questionList.size() == 0)
                System.out.println("Ta ankieta nie zawiera pytań");
        }
        catch (SQLException e){
            System.out.println("SQL Error in getAllSurveys");
        }

        return questionList;
    }
}
