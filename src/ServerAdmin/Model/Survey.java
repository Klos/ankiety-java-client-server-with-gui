package ServerAdmin.Model;

import Server.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Survey {
    public int surveyID;
    public String description;
    public int status;

    public static ArrayList<Survey> printAllSurveys(){
        ArrayList<Survey> surveyList = new ArrayList<Survey>();
        try {
            //get Surveys
            String stmt = "SELECT * FROM ankiety";
            ResultSet rsSurvey = DBUtil.dbExecuteQuery(stmt);
            Survey newSurvey = new Survey();
            System.out.println("ID\tStatus\tDescription");
            while (rsSurvey.next()) {
                newSurvey.surveyID = rsSurvey.getInt("id_ankiety");
                newSurvey.description = rsSurvey.getString("nazwa");
                newSurvey.status = rsSurvey.getInt("dostepnosc");
                System.out.println(newSurvey.surveyID +"\t|\t" + newSurvey.status + "\t|\t" + newSurvey.description);
                surveyList.add(newSurvey);
                newSurvey = new Survey();

            }
            if(surveyList.size() == 0)
                System.out.println("Baza nie zawiera żadnych ankiet");
        }
        catch (SQLException e){
            System.out.println("SQL Error in printAllSurveys");
        }

        return surveyList;
    }
}
