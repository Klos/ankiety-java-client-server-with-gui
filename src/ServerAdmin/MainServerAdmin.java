package ServerAdmin;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import ServerAdmin.Model.Answer;
import ServerAdmin.Model.Question;
import ServerAdmin.Model.Survey;
import Server.util.DBUtil;

public class MainServerAdmin {
    static Scanner userConsole = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            // sprawdź czy baza istnieje
            if (DBUtil.checkAndcreateDB()) {
                // Połącz z bazą danych
                // Jeśli nie ma takiej bazy danych to stwórz ją z pliku ankiety_wadolny.sql

                // Wyświetl menu:   zmienne do menu level1, level2 itd.
                // 1. Stwórz ankiete
                // 2. Edytuj ankiete - swtórz pytania
                // 3. Włącz/Wyłącz ankietę
                int level1 = 1;
                while (level1 != 0) {
                    System.out.println("0 - wyjscie\n1 - Stwórz ankiete\n2 - Edytuj ankiete(dodaj pytania)\n3 - włącz/wyłacz ankiete\n4 - Statystyki ankiety\n5- skasuj ankiete");
                    int level2;
                    try {
                        level2 = userConsole.nextInt();
                    } catch (java.util.InputMismatchException e) {
                        level2 = -1;
                    }
                    userConsole.nextLine();
                    if (level2 == 0)
                        level1 = 0;
                    while (level2 == 1) {
                        // 1. Stwórz ankiete
                        Survey.printAllSurveys();
                        // Pobierz nazwę aniety od użytkownika
                        System.out.println("Wprowadź nazwę ankiety, 0 - wyjście");
                        String surveyName = userConsole.nextLine();
//                System.out.println(surveyName);
                        if (surveyName.startsWith("0")) {
                            level2 = 0;
                            // Dodaj do bazy
                        } else {
                            try {
                                DBUtil.dbExecuteQuery("CALL dodaj_ankiete_do_bazy(\"" + surveyName + "\")");
                            } catch (SQLException e) {
                                System.out.println("Nie udało się dodać ankiety");
                            }
                        }

                    }
                    while (level2 == 5) {
                        //usun ankiete
                        Survey.printAllSurveys();
                        System.out.println("Wybierz ankiete do skasowania, 0 - wyjscie");
                        int surveyNo;
                        try {
                            surveyNo = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e) {
                            surveyNo = -1;
                            userConsole.nextLine();
                        }
                        if (surveyNo == 0)
                            level2 = 0;
                        else if (surveyNo > 0) {
                            try {
                                DBUtil.dbExecuteQuery("CALL skasuj_ankiete(" + surveyNo + ")");
                            } catch (SQLException e) {
                                System.out.println("Nie udało się skasować ankiety " + surveyNo);
                            }
                        }
                    }
                    while (level2 == 2) {
                        //dodaj pytania
                        // print ankiety
                        ArrayList<Survey> surveyList = Survey.printAllSurveys();
                        System.out.println("Podaj numer ankiety do edycji, 0 - wyjscie");
                        int level3;
                        try {
                            level3 = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e) {
                            level3 = -1;
                            userConsole.nextLine();
                        }
                        if (level3 == 0)
                            level2 = 0;
                        while (level3 != 0) {
                            // czek if ankieta w bazie
                            boolean surveyInDB = false;
                            for (int i = 0; i < surveyList.size(); i++) {
                                if (surveyList.get(i).surveyID == level3) {
                                    surveyInDB = true;
                                    break;
                                }
                            }
                            if (surveyInDB) {
                                //print pytania
                                Question.printAllQuestions(level3);
                                System.out.println("0 - wyjscie\n1 - Dodaj Pytanie\n2 - Edytuj pytanie(dodaj odpowiedzi)\n3- Skasuj pytanie i odpowiedzi");
                                int level4;
                                try {
                                    level4 = userConsole.nextInt();
                                } catch (java.util.InputMismatchException e) {
                                    level4 = -1;
                                }
                                userConsole.nextLine();
                                if (level4 == 0)
                                    level3 = 0;
                                while (level4 == 1) {
                                    // dodaj pytanie
                                    Question.printAllQuestions(level3);
                                    System.out.println("Wprowadź nazwę pytania, 0 - wyjście");
                                    String questionName = userConsole.nextLine();
                                    //                System.out.println(surveyName);
                                    if (questionName.startsWith("0"))
                                        level4 = 0;
                                    else {
                                        try {
                                            DBUtil.dbExecuteQuery("CALL dodaj_pytanie_do_bazy(" + level3 + " , \"" + questionName + "\")");
                                        } catch (SQLException e) {
                                            System.out.println("Nie udało się dodać pytania do bazy dla ankiety " + level3);
                                        }
                                    }
                                }
                                while (level4 == 3) {
                                    //usun pytanie
                                    Question.printAllQuestions(level3);
                                    System.out.println("Wybierz pytanie do skasowania, 0 - wyjscie");
                                    int questionNo;
                                    try {
                                        questionNo = userConsole.nextInt();
                                    } catch (java.util.InputMismatchException e) {
                                        questionNo = -1;
                                        userConsole.nextLine();
                                    }
                                    if (questionNo == 0)
                                        level4 = 0;
                                    else if (questionNo > 0) {
                                        try {
                                            DBUtil.dbExecuteQuery("CALL skasuj_pytanie(" + questionNo + ")");
                                        } catch (SQLException e) {
                                            System.out.println("Nie udało się skasować pytania " + questionNo);
                                        }
                                    }
                                }
                                while (level4 == 2) {
                                    // dodaj odpowiedzi
                                    // wybierz numer pytania
                                    ArrayList<Question> questionList = Question.printAllQuestions(level3);
                                    System.out.println("Wybierz pytanie, 0 - wyjscie");
                                    int level5;
                                    try {
                                        level5 = userConsole.nextInt();
                                    } catch (java.util.InputMismatchException e) {
                                        level5 = -1;
                                        userConsole.nextLine();
                                    }

                                    if (level5 == 0)
                                        level4 = 0;
                                    else {
                                        //czek if pytanie w bazie
                                        boolean questionInDB = false;
                                        for (int i = 0; i < questionList.size(); i++) {
                                            if (questionList.get(i).questionID == level5) {
                                                questionInDB = true;
                                                break;
                                            }
                                        }
                                        if (questionInDB) {
                                            //print odpowiedzi
                                            while (level5 != 0) {
                                                Answer.printAllAnswers(level5);
                                                System.out.println("0 - wyjscie\n1 - Dodaj Odpowiedz 2 - Usuń odpowiedź");
                                                int level6;
                                                try {
                                                    level6 = userConsole.nextInt();
                                                } catch (java.util.InputMismatchException e) {
                                                    level6 = -1;
                                                }
                                                userConsole.nextLine();
                                                if (level6 == 0)
                                                    level5 = 0;
                                                while (level6 == 1) {
                                                    //dodaj odpowiedz
                                                    Answer.printAllAnswers(level5);
                                                    System.out.println("Wprowadź nazwę odpowiedzi, 0 - wyjście");
                                                    String answerName = userConsole.nextLine();
                                                    if (answerName.startsWith("0"))
                                                        level6 = 0;
                                                        // add to DB
                                                    else {
                                                        try {
                                                            DBUtil.dbExecuteQuery("CALL dodaj_odpowiedz_do_bazy(" + level5 + " , \"" + answerName + "\")");
                                                        } catch (SQLException e) {
                                                            System.out.println("Nie udało się dodać odpowiedzi do bazy dla pytania " + level5);
                                                        }
                                                    }
                                                }
                                                while (level6 == 2) {
                                                    //usun odpowiedz
                                                    Answer.printAllAnswers(level5);
                                                    System.out.println("Wybierz odpowiedź do skasowania, 0 - wyjscie");
                                                    int answerNo;
                                                    try {
                                                        answerNo = userConsole.nextInt();
                                                    } catch (java.util.InputMismatchException e) {
                                                        answerNo = -1;
                                                        userConsole.nextLine();
                                                    }
                                                    if (answerNo == 0)
                                                        level6 = 0;
                                                    else if (answerNo > 0) {
                                                        try {
                                                            DBUtil.dbExecuteQuery("CALL skasuj_odpowiedz(" + answerNo + ")");
                                                        } catch (SQLException e) {
                                                            System.out.println("Nie udało się skasować odpowiedzi " + answerNo);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // 2. Edytuj ankiete i dodaj pytania
                        // Pobierz numer ankiety od użytkownika
                        // W pętli:
                        //      Wyświetl pytania
                        //          0 - powrót do menu głównego 1- dodaj pytanie 2- usuń pytanie
                        //      2- usuń -> Call(usun_pytanie(nr_pytania) = kasuje pytanie i wsyzstkie odpowiedzi
                        //
                        //      1- dodaj pytanie:
                        //          Pobierz treść
                        //          Wczytaj ile chcesz dodać odpowiedzi
                        //          Wczytaj odpowiedzi do listy arraylist
                        //          dodaj do bazy pytanie w return select będzie numer pytania [id]
                        //          wywołaj CALL Add_answer(num_question, tresć_odpowiedzi) dla każdej odpowiedzi z arraylist
                        //          wyzeruj araylist itd.
                        //          pokaż ponownie menu
                    }
                    while (level2 == 3) {
                        // Toggle ankieta
                        //print ankiety
                        ArrayList<Survey> surveyList = Survey.printAllSurveys();
                        System.out.println("Wprowadź numer ankiety do zmiany dostępności, 0 - wyjście");
                        int surveyNo;
                        try {
                            surveyNo = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e) {
                            surveyNo = -1;
                            userConsole.nextLine();
                        }

                        if (surveyNo == 0)
                            level2 = 0;
                        else {
                            // czek if ankieta in DB
                            boolean surveyInDB = false;
                            for (int i = 0; i < surveyList.size(); i++) {
                                if (surveyList.get(i).surveyID == surveyNo) {
                                    surveyInDB = true;
                                    break;
                                }
                            }
                            if (surveyInDB) {
                                try {
                                    DBUtil.dbExecuteQuery("CALL toggle_ankieta(" + surveyNo + ")");
                                } catch (SQLException e) {
                                    System.out.println("Nie udało się zmienić statusu ankiety");
                                }
                            }
                        }
                    }
                    while (level2 == 4) {
                        // Statysytki
                        //print ankiety
                        ArrayList<Survey> surveyList = Survey.printAllSurveys();
                        System.out.println("Wprowadź numer ankiety do wyświetlenia statystyki, 0 - wyjście");
                        int surveyNo;
                        try {
                            surveyNo = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e) {
                            surveyNo = -1;
                            userConsole.nextLine();
                        }

                        if (surveyNo == 0)
                            level2 = 0;
                        else {
                            // czek if ankieta in DB
                            boolean surveyInDB = false;
                            for (int i = 0; i < surveyList.size(); i++) {
                                if (surveyList.get(i).surveyID == surveyNo) {
                                    surveyInDB = true;
                                    break;
                                }
                            }
                            if (surveyInDB) {
                                // statystyka ankiety
                                ArrayList<Question> questionList = Question.getAllQuestions(surveyNo);
                                System.out.println("Statystyki dla ankiety " + surveyNo);
                                for (int i = 0; i < questionList.size(); i++) {
                                    System.out.println("\nPytanie nr " + questionList.get(i).questionID + " Treść: " + questionList.get(i).description);
                                    ArrayList<Answer> answerList = Answer.getAllAnswers(questionList.get(i).questionID);
                                    double sum = 0.0;
                                    for (int j = 0; j < answerList.size(); j++) {
                                        sum += (double) answerList.get(j).answerAmount;
                                    }

                                    for (int j = 0; j < answerList.size(); j++) {
                                        // System.out.println("Odpowiedź: " + answerList.get(j).description + "||| ilość odpowiedzi: " + answerList.get(j).answerAmount + "||| odpowiedzi(%): " + ((double)answerList.get(j).answerAmount * 100.0 / sum) + "%");
                                        System.out.println("odpowiedzi(%): " + ((double) answerList.get(j).answerAmount * 100.0 / sum) + "%\t||| ilość odpowiedzi: " + answerList.get(j).answerAmount + "\t||| Odpowiedź: " + answerList.get(j).description);
                                    }
                                }
                            }
                        }
                    }
                    if (level2 != 0 && level2 != 1 && level2 != 2 && level2 != 3) {
                        System.out.println("Choose correct value");
                    }
                }
            } else {
                System.out.println("Nie można utowrzyć bazy danych");
            }
        }catch (java.lang.ClassNotFoundException e){
            System.out.println("Brak DB driver!");
        }

        catch(SQLException e){
            e.printStackTrace();
        }
    }
}
