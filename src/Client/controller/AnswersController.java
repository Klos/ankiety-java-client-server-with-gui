package Client.controller;

import Client.model.Answer;
import Client.model.Question;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class AnswersController {
    private DataInputStream input = null;
    private DataOutputStream output = null;
    private Socket serverSocket = null;

    public void setServer(Socket serverSocket, DataInputStream input, DataOutputStream output){
        this.input = input;
        this.output = output;
        this.serverSocket = serverSocket;
    }

    @FXML
    ScrollPane resultsPane;
    @FXML
    private VBox vBox;
    @FXML
    private Label infoLabel;

    @FXML
    private void initialize(){

        Platform.runLater(()-> {drawStats();});
        infoLabel = new Label();
        infoLabel.setText("Ta ankieta jest pusta");
    }

    private void drawStats(){

        vBox = new javafx.scene.layout.VBox();
        vBox.getChildren().add(infoLabel);
        try {
            output.flush();
            if(RootController.surveyNo > 0) {
                output.writeUTF("GetStats");
                output.writeInt(RootController.surveyNo);
                Question question = new Question();
                ArrayList<Answer> answerList = new ArrayList<Answer>();

                while ((question.questionNo = input.readInt()) != 0) {
                    answerList.clear();
                    // Get question from server
                    question.questionDescription = input.readUTF();

                    Answer answer = new Answer();
                    int sumOfAnswers = 0;
                    // Get answers from server
                    while ((answer.originalAnswerNo = input.readInt()) != 0) {
                        answer.answerDescription = input.readUTF();
                        answer.numOfAnswers = input.readInt();
                        sumOfAnswers += answer.numOfAnswers;
                        answerList.add(answer);
                        answer = new Answer();
                    }
                    if (answerList.size() > 0) {
                        infoLabel.setVisible(false);
                        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
                        PieChart chart = new PieChart();
                        for (int i = 0; i < answerList.size(); i++) {
                            answer = answerList.get(i);
                            pieChartData.add(new PieChart.Data(answer.answerDescription + " [ " + answer.numOfAnswers + " , "
                                    + String.format("%.2f", ((double) answer.numOfAnswers * 100.00 / (double) sumOfAnswers))
                                    + "% ]", answer.numOfAnswers));
                        }
                        chart.setData(pieChartData);
                        pieChartData.removeIf(data -> data.getPieValue() == 0);
                        //chart.setPrefSize(500,500);
                        chart.setTitle(question.questionDescription);
                        vBox.getChildren().add(chart);
                    }
                }
            }
            else{
                infoLabel.setText("Nie wybrałeś ankiety!");
                infoLabel.setTextFill(Color.color(1, 0, 0));
            }
            resultsPane.setContent(vBox);
        } catch (java.lang.NullPointerException e) {
            System.out.println("NullPointerException: " + e.getMessage());
        } catch (UnknownHostException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
            if (e.getMessage().equals("Connection refused: connect"))
                System.out.println("Server is unavailable");
            else
                System.out.println(e.getMessage());
        }
    }
}
