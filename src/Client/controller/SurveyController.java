package Client.controller;
//javaFX
import Client.model.Survey;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;


public class SurveyController {
    private DataInputStream input = null;
    private DataOutputStream output = null;
    private Socket serverSocket = null;
    private Survey currentSurveyNo;
    private ArrayList<Survey> surveyList;

    public void setServer(Socket serverSocket, DataInputStream input, DataOutputStream output){
        this.input = input;
        this.output = output;
        this.serverSocket = serverSocket;
    }

    @FXML
    private TextArea consoleOutput;
    @FXML
    private TextField userInput;
    @FXML
    private Button sendBtn;
    @FXML
    private Button getSurveysBtn;

    // to comunicate with rootLayout
    private final ReadOnlyObjectWrapper<Survey> currentSurvey = new ReadOnlyObjectWrapper<>();

    public ReadOnlyObjectProperty<Survey> currentSurveyProperty() {
        return currentSurvey.getReadOnlyProperty() ;
    }

    @FXML
    private void initialize(){
        surveyList = new ArrayList<Survey>();

        Platform.runLater(()-> {getSurveysBtn.fire();});

    }

    //Set information to Text Area
    @FXML
    private void setInfoToTextArea (String infoText) {
        consoleOutput.setText(infoText);
    }
    @FXML
    private void appendInfoToTextArea (String infoText) {
        consoleOutput.appendText("\n" + infoText);
    }
    @FXML
    private void getSurveys(ActionEvent actionEvent){
        try {
            // Get surveys list
            output.writeUTF("GetSurveys");
            surveyList.clear();
            Survey survey = new Survey();
            while((survey.surveyNo = input.readInt()) != 0){
                survey.surveyDescription = input.readUTF();
                surveyList.add(survey);
                survey = new Survey();
            }
            setInfoToTextArea("Lista dostępnych ankiet");
            // Print survey's list
            for(int i = 0; i < surveyList.size(); i++){
                appendInfoToTextArea(surveyList.get(i).surveyNo + "\t| " + surveyList.get(i).surveyDescription);
            }
            if(surveyList.size() == 0)
                appendInfoToTextArea("Lista jest pusta");
            appendInfoToTextArea("Wybierz ankietę");

        } catch (IOException e) {
//            e.printStackTrace();
            appendInfoToTextArea("Nie udało się pobrać ankiet z serwera");
            appendInfoToTextArea("Sprawdź, czy dane serwera są poprawne");
            appendInfoToTextArea("Serwer mógł przejść w stan offline");
        } catch (Exception e) {
//            e.printStackTrace();
            appendInfoToTextArea("Nie udało się pobrać ankiet z serwera");
            appendInfoToTextArea("Sprawdź, czy dane serwera są poprawne");
            appendInfoToTextArea("Serwer mógł przejść w stan offline");
        }
    }
    // choose Survey
    @FXML
    private void chooseSurvey(ActionEvent actionEvent) {
        if(surveyList.size() > 0) {
            String tmp = userInput.getText();
            boolean correctNumber = false;
            try {
                int number = Integer.parseInt(tmp);
                int i;
                // Check if number in Database
                for (i = 0; i < surveyList.size(); i++) {
                    if (surveyList.get(i).surveyNo == number) {
                        correctNumber = true;
                        break;
                    }
                }
                if (correctNumber == false)
                    appendInfoToTextArea("Zły numer ankiety");
                else {
                    currentSurveyNo = new Survey();
                    currentSurveyNo.surveyNo = number;
                    currentSurveyNo.surveyDescription = surveyList.get(i).surveyDescription;
                    currentSurvey.set(currentSurveyNo);
                    appendInfoToTextArea("Udało się zmienić numer ankiety na: " + number);
                }
            } catch (Exception e) {
                appendInfoToTextArea("Zły numer ankiety");
            }
        }
        else{
            appendInfoToTextArea("Lista jest pusta!!");
        }
    }
}
