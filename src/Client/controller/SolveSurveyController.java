package Client.controller;

import Client.model.Answer;
import Client.model.Question;
import Client.model.Survey;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class SolveSurveyController {
    private DataInputStream input = null;
    private DataOutputStream output = null;
    private Socket serverSocket = null;

    private boolean surveyIsRunning = false;
    Question question;
    ArrayList<Answer> answerList;


    public void setServer(Socket serverSocket, DataInputStream input, DataOutputStream output){
        this.input = input;
        this.output = output;
        this.serverSocket = serverSocket;
    }

    @FXML
    private TextArea consoleArea;
    @FXML
    private TextField answerArea;
    @FXML
    private Button sendAnswerBtn;
    @FXML
    private Button startSurveyBtn;

    @FXML
    private void initialize(){
        consoleArea.setText("Ankieta numer: " + RootController.surveyNo);
//        question = new Question();
//        answerList = new ArrayList<Answer>();
    }

    @FXML
    private void sendAnswer(ActionEvent actionEvent){
        if(surveyIsRunning){
            boolean correctValue = false;
            String userAnswer = answerArea.getText();
            answerArea.clear();
            int answerNo;
            int originalAnswerNo = 0;
            try{
                answerNo = Integer.parseInt(userAnswer);

                for (int i = 0; i < answerList.size(); i++) {
                    if (answerList.get(i).answerNo == answerNo) {
                        correctValue = true;
                        originalAnswerNo = answerList.get(i).originalAnswerNo;
                        break;
                    }
                }
                if (correctValue == false) {
                    consoleArea.appendText("\nZły numer odpowiedzi, wybierz poprawny numer odpowiedzi");
                }
                else{
                    output.writeInt(originalAnswerNo);
                    consoleArea.appendText("\nTwoja odpowiedź: " + answerNo + "\n");
                    answerList.clear();
                    getNextQuestion();
                }

            }
            catch (java.lang.NumberFormatException e){
                consoleArea.appendText("\nZły numer odpowiedzi, wybierz poprawny numer odpowiedzi");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void showAnswerList() throws IOException {
        consoleArea.appendText("\n" + question.questionDescription);
        for(int i = 0; i < answerList.size(); i++){
            consoleArea.appendText("\n" + answerList.get(i).answerNo + "\t|\t" + answerList.get(i).answerDescription);
        }
        if(answerList.size() == 0) {
            consoleArea.appendText("\nLista odpowiedzi dla tego pytania jest pusta");
            getNextQuestion();
        }
//        consoleArea.setScrollTop(Double.MAX_VALUE);
//        consoleArea.getScrollTop();
          consoleArea.appendText("\n");
    }
    private void getNextQuestion() throws IOException {
        question = new Question();
        answerList = new ArrayList<Answer>();
        if ((question.questionNo = input.readInt()) != 0) {
            // Get question from server
            question.questionDescription = input.readUTF();
            //System.out.println(question.questionDescription);

            Answer answer = new Answer();
            int answerIterator = 1;

            // Get answers from server
            while ((answer.originalAnswerNo = input.readInt()) != 0) {
                answer.answerDescription = input.readUTF();
                answer.answerNo = answerIterator;
                answerIterator++;
                answerList.add(answer);
                answer = new Answer();
            }
            showAnswerList();
        }
        else {
            surveyIsRunning = false;
            consoleArea.appendText("\nKONIEC ANKIETY\nGRATULACJE! ODPOWIEDZIAŁEŚ NA WSZYTKIE PYTANIA");
        }
    }

    @FXML
    private void startSurvey() {
        if (RootController.surveyNo != 0) {
            // pobierz 1 pytanie i odpowiedzi i wyswietl
            surveyIsRunning = true;
            startSurveyBtn.setVisible(false);

            try {
                // Send survey's number to server
                output.flush();
                output.writeUTF("StartSurvey");
                output.writeInt(RootController.surveyNo);

                getNextQuestion();
            } catch (java.lang.NullPointerException e) {
                System.out.println("NullPointerException: " + e.getMessage());
            } catch (UnknownHostException e) {
                System.out.println("Socket: " + e.getMessage());
            } catch (IOException e) {
                System.out.println("IO: " + e.getMessage());
                if (e.getMessage().equals("Connection refused: connect"))
                    System.out.println("Server is unavailable");
                else
                    System.out.println(e.getMessage());
            }
        }
        else{
            consoleArea.setText("Nie wybrałeś ankiety!");
        }
    }
}
