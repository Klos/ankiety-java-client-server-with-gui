package Client.controller;

import Client.MainClient;
import Client.model.Survey;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static javafx.application.Platform.exit;

public class RootController {
    public static int surveyNo = 0;
    String serverIP;
    int serverPort;
    Socket serverSocket = null;
    DataInputStream input = null;
    DataOutputStream output = null;


    @FXML
    private TextField serverIPField;
    @FXML
    private TextField serverPortField;
    @FXML
    private Button chooseSurveyBtn;
    @FXML
    private Button solveSurveyBtn;
    @FXML
    private Button showResultsBtn;
    @FXML
    private Button getConnectionBtn;
    @FXML
    private Button endSurveyBtn;
    @FXML
    private Button endProgramBtn;
    @FXML
    private Label serverIPLabel;
    @FXML
    private Label serverPortLabel;
    @FXML
    private Label infoLabel;
    @FXML
    private Label infoNrLabel;
    @FXML
    private Label surveyNoLabel;
    @FXML
    private Label surveyDescLabel;
    @FXML
    private BorderPane rootBorderPane;

    @FXML
    private void connectToServer(ActionEvent actionEvent) {
        serverIP = serverIPField.getText();
        String port = serverPortField.getText();

        if(!port.isEmpty())
            serverPort = Integer.parseInt(serverPortField.getText());
        else
            serverPort = 0;
        try {
            serverSocket = new Socket(serverIP, serverPort);
            // Initialize buffers
            input = new DataInputStream(serverSocket.getInputStream());
            output = new DataOutputStream(serverSocket.getOutputStream());

            // hide buttons
            chooseSurveyBtn.setVisible(true);
            solveSurveyBtn.setVisible(true);
            showResultsBtn.setVisible(true);
            surveyNoLabel.setVisible(true);
            endProgramBtn.setVisible(true);
            infoNrLabel.setVisible(true);
            surveyDescLabel.setVisible(true);
            getConnectionBtn.setVisible(false);
            serverIPLabel.setVisible(false);
            serverPortLabel.setVisible(false);
            infoLabel.setVisible(false);
            serverIPField.setVisible(false);
            serverPortField.setVisible(false);
        } catch (IOException e) {
            infoLabel.setText("Nie udało się połączyć z serwerem");
            infoLabel.setVisible(true);
        } catch (java.lang.RuntimeException e){
            infoLabel.setText("Niepoprawne dane serwera");
            infoLabel.setVisible(true);
        }
    }

    public void setSurveyNo(int number){
        RootController.surveyNo = number;
        surveyNoLabel.setText(Integer.toString(number));
    }
    public void setSurveyLabel(String desc){
        surveyDescLabel.setText(desc);
    }

    @FXML
    private void chooseSurvey(ActionEvent actionEvent) {
        //load Survey Layout
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(MainClient.class.getResource("view/SurveyLayout.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            SurveyController controller = loader.getController();
            controller.setServer(serverSocket, input ,output);
            controller.currentSurveyProperty().addListener((number) -> {

                Survey tmp = controller.currentSurveyProperty().get();
                setSurveyNo(tmp.surveyNo);
                setSurveyLabel(tmp.surveyDescription);
            });
            rootBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @FXML
    private void solveSurvey(ActionEvent actionEvent) {
        //load solveSurvey Layout
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(MainClient.class.getResource("view/SolveSurvey.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            SolveSurveyController controller = loader.getController();
            controller.setServer(serverSocket, input ,output);
            endSurveyBtn.setVisible(true);
            endProgramBtn.setVisible(false);
            chooseSurveyBtn.setVisible(false);
            solveSurveyBtn.setVisible(false);
            showResultsBtn.setVisible(false);
            rootBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    private void endSurvey(ActionEvent actionEvent) {
        try {
            output.writeInt(0);
            showResultsBtn.fire();
        } catch (IOException e) {
//            e.printStackTrace();
        } finally {
            endSurveyBtn.setVisible(false);
            endProgramBtn.setVisible(true);
            chooseSurveyBtn.setVisible(true);
            solveSurveyBtn.setVisible(true);
            showResultsBtn.setVisible(true);
        }
    }

    @FXML
    private void showResults(ActionEvent actionEvent) {
        //load showSurveyResults Layout
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(MainClient.class.getResource("view/AnswersLayout.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            AnswersController controller = loader.getController();
            controller.setServer(serverSocket, input ,output);
            rootBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    private void endProgram(ActionEvent actionEvent) {
        try {
            output.writeUTF("ENDCONNECTION");
            output.flush();

        } catch (IOException e) {
//            e.printStackTrace();
        }
        finally {
            if(!serverSocket.isClosed()){
                try {
                    serverSocket.close();
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
            exit();
        }
    }
}
