package Client.model;

public class Survey {
    public int surveyNo;
    public String surveyDescription;

    @Override
    public String toString() {
        return ("\n" + surveyNo + "\t|\t" + surveyDescription);
    }
}
