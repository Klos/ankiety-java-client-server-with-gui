package Server.model;

import Server.util.DBUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerListener extends Thread {
    int serverPort;
    ExecutorService executor;
    ServerSocket listenSocket = null;
    public static boolean isServerRunning = false;

    public ServerListener(int serverPort){
        this.serverPort = serverPort;
        executor = Executors.newCachedThreadPool();
    }
    public void stopServer(){
        executor.shutdown();
        isServerRunning = false;
        if(listenSocket != null)
            try {
                listenSocket.close();
                System.out.println("Server socket closed!");
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
    @Override
    public void run() {
        try{
            listenSocket = new ServerSocket(serverPort);
            System.out.println("Server starts listening...");
            isServerRunning = true;
            while (!Thread.currentThread().isInterrupted()) {
                Socket clientSocket = listenSocket.accept();
                //ServerConnection c = new ServerConnection(clientSocket);
                executor.execute(new ServerConnection(clientSocket));
            }
        }
        catch(IOException e) {
            System.out.println("Listen : "+e.getMessage());
        }
        finally {
            if(listenSocket != null)
                try {
                    listenSocket.close();
                    System.out.println("Server socket closed!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
