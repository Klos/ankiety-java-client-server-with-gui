package Server.model;

import java.io.*;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;

import Server.util.DBUtil;

public class ServerConnection implements Runnable {
    DataInputStream input;
    DataOutputStream output;
    Socket clientSocket;
    private  int userNIU = 0;

    public ServerConnection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            // Init buffers
            input = new DataInputStream( clientSocket.getInputStream());
            output = new DataOutputStream( clientSocket.getOutputStream());
//            this.start();
        }
        catch(IOException e) {
            System.out.println("ServerConnection problem: "+e.getMessage());
        }
    }

    public void run() {
        try {
            //CreateNIU
            String stmt;
            System.out.println("NEW CONNECTION");
            String command = "START";
            // COMMAND LISTEN
            while(!command.equals("ENDCONNECTION")){
                command = input.readUTF();
                System.out.println("NIU" + userNIU + " Command: " + command);
                if(command.equals("GetSurveys")) {
                    //get Surveys
                    stmt = "SELECT * FROM ankiety WHERE dostepnosc = '1'";
                    ResultSet rsSurvey = DBUtil.dbExecuteQuery(stmt);

                    // Send number + survey description to client
                    int numOfSurveys = 0;
                    int surveyNo;
                    String surveyDescription;
                    while (rsSurvey.next()) {
                        surveyNo = rsSurvey.getInt("id_ankiety");
                        surveyDescription = rsSurvey.getString("nazwa");
                        output.writeInt(surveyNo);
                        output.writeUTF(surveyDescription);
                        numOfSurveys++;
                    }
                    output.writeInt(0);
                    output.flush();
                }
                if(command.equals("StartSurvey")) {
                    stmt = "CALL pobierz_NIU();";
                    ResultSet rsNIU = DBUtil.dbExecuteQuery(stmt);
                    rsNIU.next();
                    userNIU = rsNIU.getInt("niu");
                    System.out.println("NIU: " + userNIU);
                     //Get survey's number from client
                    int surveyId = input.readInt();
                    System.out.println("Clients choose " + surveyId);

                    // Get surveys questions from DB
                    stmt = "SELECT * FROM ankiety_pytania ap LEFT JOIN pytania p ON ap.id_pytania = p.id_pytania WHERE id_ankiety =" + surveyId;
                    ResultSet rsQuestions = DBUtil.dbExecuteQuery(stmt);

                    // In loop while end of survey:
                    int numOfQuestions = 0;
                    int questionNo;
                    String questionDescription;

                    int numOfAnswers;
                    int answerNo;
                    String answerDescription;

                    int clientAnswer = 5;

                    while(rsQuestions.next()){
                        questionNo = rsQuestions.getInt("id_pytania");
                        questionDescription = rsQuestions.getString("tresc");

                        // Get question answers from DB
                        stmt = "SELECT * FROM pytania_odpowiedzi po LEFT JOIN odpowiedzi o ON po.id_odpowiedzi = o.id_odpowiedzi WHERE id_pytania = " + questionNo;
                        ResultSet rsAnswers = DBUtil.dbExecuteQuery(stmt);
                        numOfAnswers = 0;

                        // send question to client
                        output.writeInt(questionNo);
                        output.writeUTF(questionDescription);

                        while(rsAnswers.next()){
                            answerNo = rsAnswers.getInt("id_odpowiedzi");
                            answerDescription = rsAnswers.getString("odpowiedz");
                            //      send answers to client
                            output.writeInt(answerNo);
                            output.writeUTF(answerDescription);
                            numOfAnswers++;
                        }
                        // End of answers - send '0'
                        output.writeInt(0);

                        //      Get answer from client
                        if (numOfAnswers > 0) {
                            clientAnswer = input.readInt();
                            System.out.println("Odpowiedz klienta: " + clientAnswer);
                            if(clientAnswer == 0){
                                System.out.println("Koniec ankiety");
                                break;
                            }
                            // Put answer to DB
                            stmt = "CALL dodaj_odpowiedz(" + userNIU+", " + clientAnswer + ")";
                            ResultSet rsAddAnswer = DBUtil.dbExecuteQuery(stmt);
                            if(rsAddAnswer.next())
//                                System.out.println(rsAddAnswer.getString("Info"));
                                System.out.println("Odpowiedz dodana do bazy");
                        }
                        numOfQuestions++;
                    }

                    // Send '0' to client - end of survey
                    if(clientAnswer != 0)
                        output.writeInt(0);

                    System.out.println("Koniec ankiety");
                    userNIU = 0;
                }
                if(command.equals("CommitAnswers")) {
                    // not used


                }
                if(command.equals("GetStats")) {
                    //Get survey's number from client
                    int surveyId = input.readInt();
                    System.out.println("Clients choose " + surveyId);
                    if(surveyId > 0) {
                        // Get surveys questions from DB
                        stmt = "SELECT * FROM ankiety_pytania ap LEFT JOIN pytania p ON ap.id_pytania = p.id_pytania WHERE id_ankiety =" + surveyId;
                        ResultSet rsQuestions = DBUtil.dbExecuteQuery(stmt);

                        // In loop while end of survey:
                        int questionNo;
                        String questionDescription;


                        int answerNo;
                        int answerStats;
                        String answerDescription;

                        while (rsQuestions.next()) {
                            questionNo = rsQuestions.getInt("id_pytania");
                            questionDescription = rsQuestions.getString("tresc");

                            // Get question answers from DB
                            stmt = "SELECT * FROM pytania_odpowiedzi po LEFT JOIN odpowiedzi o ON po.id_odpowiedzi = o.id_odpowiedzi WHERE id_pytania = " + questionNo;
                            ResultSet rsAnswers = DBUtil.dbExecuteQuery(stmt);

                            // send question to client
                            output.writeInt(questionNo);
                            if(questionDescription.isEmpty())
                                output.writeUTF("Brak tresci pytania");
                            else
                                output.writeUTF(questionDescription);

                            while (rsAnswers.next()) {
                                answerNo = rsAnswers.getInt("id_odpowiedzi");
                                answerDescription = rsAnswers.getString("odpowiedz");
                                answerStats = rsAnswers.getInt("ilosc_odpowiedzi");
                                //      send answers to client
                                output.writeInt(answerNo);
                                if (answerDescription.isEmpty())
                                    output.writeUTF("Pusta odpowiedz");
                                else
                                    output.writeUTF(answerDescription);
                                output.writeInt(answerStats);
                            }
                            // End of answers - send '0'
                            output.writeInt(0);
                        }
                        // Send '0' to client
                        output.writeInt(0);
                        System.out.println("End of stats");
                    }
                }
                // END OF CONNECTION
                output.flush();
            }
            System.out.println("END OF CONNECTION WITH NIU: " + userNIU);
        }
        catch(java.lang.NullPointerException e){
            System.out.println("NullPointerException: " + e.getMessage());
        }
        catch(IOException e) {
            System.out.println("IO: " + e.getMessage());
        }
        catch (SQLException e) {
            System.out.println("Error in SQL: " + e);
            //Return exception
        }
        finally {
            try {
                clientSocket.close();
            }
            catch (IOException e){
                System.out.println("Error while closing connection: " + e.getMessage());
            }
        }
    }

}
