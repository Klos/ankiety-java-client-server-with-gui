package Server.controller;

import Server.MainServer;
import Server.model.ServerListener;
import Server.util.DBUtil;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.sql.SQLException;

public class RootController {
    public static ServerListener serverListenerHolder;
    public static boolean safeServerClose = false;

    @FXML
    private Button startServerBtn;
    @FXML
    private Button switchSurveysBtn;
    @FXML
    private Button createDBDumpBtn;
    @FXML
    private Button loadDBDumpBtn;
    @FXML
    private Button endProgramBtn;
    @FXML
    private TextField serverPortField;
    @FXML
    private TextField dbUsernameField;
    @FXML
    private TextField dbSIDField;
    @FXML
    private TextField dbPortField;
    @FXML
    private TextField dbIPField;
    @FXML
    private TextField dbPassField;
    @FXML
    private Label dbNameLabel;
    @FXML
    private Label dbPortLabel;
    @FXML
    private Label dbIPLabel;
    @FXML
    private Label passwordLabel;
    @FXML
    private Label usernameLabel;
    @FXML
    private Label dbSettingsLabel;
    @FXML
    private Label serverPortLabel;
    @FXML
    private Label infoLabel;
    @FXML
    private BorderPane rootBorderPane;

    @FXML
    private void startServer(ActionEvent actionEvent) {
    try {
        DBUtil.setLoginData(dbUsernameField.getText(), dbPassField.getText(), dbIPField.getText(), dbPortField.getText(), dbSIDField.getText());
        if(DBUtil.checkAndcreateDB()) {
            int serverPort = Integer.parseInt(serverPortField.getText());
            serverListenerHolder = new ServerListener(serverPort);
            serverListenerHolder.start();

            // Hide show buttons/ labels
            switchSurveysBtn.setVisible(true);
//            createDBDumpBtn.setVisible(true);
//            loadDBDumpBtn.setVisible(true);
            endProgramBtn.setVisible(true);

            startServerBtn.setVisible(false);
            serverPortField.setVisible(false);
            dbUsernameField.setVisible(false);
            dbSIDField.setVisible(false);
            dbPortField.setVisible(false);
            dbIPField.setVisible(false);
            dbPassField.setVisible(false);
            dbSettingsLabel.setVisible(false);
            serverPortLabel.setVisible(false);
            usernameLabel.setVisible(false);
            passwordLabel.setVisible(false);
            dbIPLabel.setVisible(false);
            dbPortLabel.setVisible(false);
            dbNameLabel.setVisible(false);
            infoLabel.setVisible(false);
        }
        else{
            infoLabel.setText("Nie udało się stworzyć bazy danych, sprawdź czy dobrze wpisałeś dane bazy");
            infoLabel.setVisible(true);
        }

    } catch (java.lang.NumberFormatException e){
//        e.printStackTrace();
        infoLabel.setText("NumberFormatException: Niepoprawnie wpisane wartości");
        infoLabel.setVisible(true);
    } catch (java.lang.IllegalArgumentException e){
//        e.printStackTrace();
        infoLabel.setText("IllegalArgumentException: Niepoprawnie wpisane wartości");
        infoLabel.setVisible(true);
    } catch(SQLException e){
//        e.printStackTrace();
        infoLabel.setText("SQLException: Niepoprawnie wpisane wartości bazy danych");
        infoLabel.setVisible(true);
    } catch (ClassNotFoundException e){
//        e.printStackTrace();
        infoLabel.setText("ClassNotFoundException: Gdzie jest mysql-connector lib? Zrestartuj aplikację!");
        infoLabel.setVisible(true);
    }
    catch (Exception e){
//        e.printStackTrace();
        infoLabel.setText("Niepoprawnie wpisane wartości lub serwer MySQL nie włączony");
        infoLabel.setVisible(true);
    }

    }

    @FXML
    private void switchSurveys(ActionEvent actionEvent) {
        //load Switch Survey Layout
        FXMLLoader loader  = new FXMLLoader();
//        loader.setLocation(getClass().getResource("../view/SwitchSurvey.fxml")); // It is not working in JAR file
        loader.setLocation(MainServer.class.getResource("view/SwitchSurvey.fxml"));
        Parent root = null;
        try {
            root = loader.load();
            rootBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void createDBDump(ActionEvent actionEvent) {


    }

    @FXML
    private void loadDBDump(ActionEvent actionEvent) {


    }

    @FXML
    private void endProgram(ActionEvent actionEvent) {
        safeServerClose = true;
        if(!serverListenerHolder.isInterrupted()) {
            serverListenerHolder.stopServer();
            serverListenerHolder.interrupt();
        }
        Platform.exit();
    }
}
