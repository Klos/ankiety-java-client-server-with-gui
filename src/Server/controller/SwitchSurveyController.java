package Server.controller;

import Server.model.Survey;
import Server.util.DBUtil;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.sql.SQLException;
import java.util.ArrayList;


public class SwitchSurveyController {
    ArrayList<Survey> surveyList;

    @FXML
    private TextArea consoleOutput;
    @FXML
    private TextField userInput;
    @FXML
    private Button sendBtn;
    @FXML
    private Button getSurveysBtn;

    @FXML
    private void initialize(){
        Platform.runLater(()-> {getSurveysBtn.fire();});
    }

    //Set information to Text Area
    @FXML
    private void setInfoToTextArea (String infoText) {
        consoleOutput.setText(infoText);
    }
    @FXML
    private void appendInfoToTextArea (String infoText) {
        consoleOutput.appendText("\n" + infoText);
    }
    @FXML
    private void getSurveys(ActionEvent actionEvent){
        try {
            surveyList = Survey.getAllSurveys();
            if(surveyList.size() == 0)
                appendInfoToTextArea("Lista Ankiet jest pusta");
            else {
                appendInfoToTextArea( "SURVEY ID" + "  \t|  " + "STATUS" + "  \t|  " + "OPIS\n");
                for (int i = 0; i < surveyList.size(); i++)
                    appendInfoToTextArea(surveyList.get(i).surveyID + "  \t|  " + surveyList.get(i).status + "  \t|  " + surveyList.get(i).description + "\n");
                appendInfoToTextArea("Wybierz ankietę do zmiany dostępności");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            appendInfoToTextArea("Nie udało się pobrać ankiet z bazy");
            appendInfoToTextArea("Sprawdź, czy dane bazy są poprawne");
        }
    }
    // choose Survey
    @FXML
    private void switchSurvey(ActionEvent actionEvent) {
        if(surveyList.size() > 0) {
            String tmp = userInput.getText();
            userInput.clear();
            boolean correctNumber = false;
            try {
                int number = Integer.parseInt(tmp);
                int i;
                // Check if number in Database
                for (i = 0; i < surveyList.size(); i++) {
                    if (surveyList.get(i).surveyID == number) {
                        correctNumber = true;
                        break;
                    }
                }
                if (correctNumber == false)
                    appendInfoToTextArea("Zły numer ankiety");
                else {
                    try {
                        DBUtil.dbExecuteQuery("CALL toggle_ankieta(" + number + ")");
                        appendInfoToTextArea("Zmieniono status ankiety: " + number);
                    } catch (SQLException e) {
                        System.out.println("Nie udało się zmienić statusu ankiety");
                    }
                    Platform.runLater(()-> {getSurveysBtn.fire();});
                }
            } catch (Exception e) {
                appendInfoToTextArea("Zły numer ankiety");
            }
        }
        else{
            appendInfoToTextArea("Lista jest pusta!!");
        }

    }
}
