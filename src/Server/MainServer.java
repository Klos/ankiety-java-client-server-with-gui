package Server;

import Server.controller.RootController;
import Server.model.ServerListener;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class MainServer extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    //This is our PrimaryStage (It contains everything)
    private Stage primaryStage;

    //This is the BorderPane of RootLayout
    private BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) throws Exception{

        //1) Declare a primary stage (Everything will be on this stage)
        this.primaryStage = primaryStage;

        //Set a title for primary stage
        this.primaryStage.setTitle("Surveys Server");

        //2) Initialize RootLayout
        initRootLayout();

    }
    @Override
    public void stop(){
        System.out.println("App is closing");
        if(ServerListener.isServerRunning) {
            if (RootController.safeServerClose == false) {
                Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
                exitAlert.setTitle("Server Shutdown");
                exitAlert.setHeaderText("UWAGA Wciskając X kasujesz wszystkie połączenia serwera");
                exitAlert.setContentText("Następnym razem wybierz \"zamknij program\", zamiast wciskać X. Czy chcesz zaczekać na zakończenie wszsytkich połączeń?");

                ButtonType buttonTypeOne = new ButtonType("Zaczekaj");
                ButtonType buttonTypeCancel = new ButtonType("Zamknij wszystkie połączenia", ButtonBar.ButtonData.CANCEL_CLOSE);
                exitAlert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);
                Optional<ButtonType> result = exitAlert.showAndWait();
                if (result.get() == buttonTypeOne) {
                    System.out.println("Wait till all threads are ended");
                    RootController.serverListenerHolder.stopServer();
                } else {
                    System.out.println("TERMINATE ALL THREADS!");
                    System.exit(0);
                }
            }
        }
    }

    //Initializes the root layout.
    public void initRootLayout() {
        try {
            //First, load root layout from RootLayout.fxml
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainServer.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            //Second, show the scene containing the root layout.
            Scene scene = new Scene(rootLayout); //We are sending rootLayout to the Scene.
            primaryStage.setScene(scene); //Set the scene in primary stage.

            //Third, show the primary stage
            primaryStage.setFullScreenExitHint("Survey Server");
//            primaryStage.setFullScreen(true);
//            primaryStage.setAlwaysOnTop(true);

            primaryStage.show(); //Display the primary stage
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}