package Server.util;

import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;

public class DBUtil {
    // Declare JDBC Driver
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";

    // Connection
    private static Connection conn = null;

    // Login data
    private static String username = "root";
    private static String pass = "";
    private static String ip = "localhost";
    private static String port = "3306";
    private static String sid = "ankiety_wadolny";

    //Connection String
    private static String connStr = String.format("jdbc:mysql://%s:%s/%s", ip, port, sid);
    // That format is needed for calling stored procedures
    private static String connStrLogin = String.format("jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=utf-8&user=%s&password=%s", ip, port, sid, username, pass);
    private static String connStrLoginNoDB = String.format("jdbc:mysql://%s:%s/?useUnicode=true&characterEncoding=utf-8&user=%s&password=%s", ip, port, username, pass);


    public static void setLoginData(String vUsername, String vPass, String vIp, String vPort, String vSid){
        username = vUsername;
        pass = vPass;
        ip = vIp;
        port = vPort;
        sid = vSid;

        //Connection String
        connStr = String.format("jdbc:mysql://%s:%s/%s", ip, port, sid);
        // That format is needed for calling stored procedures
        connStrLogin = String.format("jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=utf-8&user=%s&password=%s", ip, port, sid, username, pass);
        connStrLoginNoDB = String.format("jdbc:mysql://%s:%s/?useUnicode=true&characterEncoding=utf-8&user=%s&password=%s", ip, port, username, pass);
    }
    // Connect to DB
    public static void dbConnect() throws SQLException {
        // setting Oracle driver
        try {
            Class.forName(DB_DRIVER);
//            System.out.println(connStr);
        } catch (ClassNotFoundException e) {
            System.err.printf("Where is your JDBC driver?");
            e.printStackTrace();
        }
//        System.out.println("DB Driver Registered!");

        // Establish the Oracle Connection
        try {
            conn = DriverManager.getConnection(connStrLogin);
        } catch (SQLException e) {
            System.err.println("Connection failed!");
            e.printStackTrace();
        }
    }
    // Connect to MySQL
    public static void mysqlConnect() throws SQLException, ClassNotFoundException {
        // setting Oracle driver
        try {
            Class.forName(DB_DRIVER);
//            System.out.println(connStr);
        } catch (ClassNotFoundException e) {
            System.err.printf("Where is your DB driver?");
            throw e;
//            e.printStackTrace();
        }
        System.out.println("DB Driver Registered!");

        // Establish the MYSQL Connection
        try {
            conn = DriverManager.getConnection(connStrLoginNoDB);
            System.out.println("Connected");
        } catch (SQLException e) {
            System.err.println("Connection failed!");
            e.printStackTrace();
        }
    }

    // Close connection
    public static void dbDisconnect() throws SQLException {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            System.err.println("Error while closing the connection!");
            e.printStackTrace();
        }
    }

    // DB Execute Query Operation (select)
    public static ResultSet dbExecuteQuery(String queryStmt) throws SQLException {
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSet cachedRowSet = null;

        try {
            // Connect to DB
            dbConnect();
//            System.out.println("Select statement: " + queryStmt);

            // Create statement
            stmt = conn.createStatement();
//            stmt.execute("SET NAMES 'unicode'");

            // Execute operation
            resultSet = stmt.executeQuery(queryStmt);

            // Cached Row Set Implementation
            // In order to prevent "java.sql.SQLRecoverableException: Closed Connection: next error
            cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
        } catch (SQLException e) {
            System.err.println("Error at executing query");
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            // Close connection
//            conn.close();
            dbDisconnect();
        }
        return cachedRowSet;
    }
    public static void dbExecuteUpdate(String sqlStmt) throws SQLException {
        Statement stmt = null;
        try {
            dbConnect();
            stmt = conn.createStatement();
            stmt.executeUpdate(sqlStmt);
        } catch (SQLException e) {
            System.err.println("Error while executing update statement");
            e.printStackTrace();
        } finally {
            if(stmt != null) {
                stmt.close();
            }
            dbDisconnect();
        }
    }

    public static boolean checkAndcreateDB() throws SQLException, ClassNotFoundException {
        try {
            if (checkIfDBExist(sid) == false) {
                if (createDB())
                    return createTables();
                else
                    return false;
            } else
                return true;
        }
        catch(ClassNotFoundException e) {
            throw e;
        }
    }


    public static boolean createDB() throws SQLException, ClassNotFoundException{
        // create only db named like sid
        try {
            DBUtil.mysqlConnect();
            String sqlStmt = "CREATE DATABASE " + sid;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sqlStmt);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e){
            System.err.println("NO DB DRIVER!");
            throw e;
        }
        finally{
            try {
                DBUtil.dbDisconnect();
            } catch (SQLException e) {
                throw e;
//              e.printStackTrace();
            }
        }
    }
    public static boolean createTables(){
        final String DBSuperString[] = {
                "CREATE PROCEDURE `dodaj_ankiete_do_bazy` (`vNazwa` VARCHAR(255))  begin INSERT INTO ankiety(id_ankiety, nazwa) VALUES(NULL, vNazwa); SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"dodaj_ankiete_do_bazy\" as \"Fun\",\"Ankieta dodana do bazy\" as \"Info\"; end",
                "CREATE PROCEDURE `dodaj_odpowiedz` (`vNIU` INT, `vOdpowiedz` INT)  begin INSERT INTO odpowiedzi_uzytkownicy(id_odpowiedzi, id_uzytkownika) VALUES(vOdpowiedz, vNIU); UPDATE odpowiedzi SET ilosc_odpowiedzi = ilosc_odpowiedzi + 1 WHERE id_odpowiedzi = vOdpowiedz; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"dodaj_odpowiedz\" as \"Fun\",\"Odpowiedź dodana do bazy\" as \"Info\"; end",
                "CREATE PROCEDURE `dodaj_odpowiedz_do_bazy` (`vPytanieNO` INT, `vTresc` VARCHAR(255)) begin SET @n1 = (SELECT tresc from pytania where id_pytania = vPytanieNo); if (@n1 is NOT NULL) then SET @n2 = (SELECT max(id_odpowiedzi) from odpowiedzi) + 1; INSERT INTO odpowiedzi(id_odpowiedzi, odpowiedz, ilosc_odpowiedzi) VALUES(@n2, vTresc, 0); INSERT INTO pytania_odpowiedzi(id_pytania, id_odpowiedzi) VALUES(vPytanieNO, @n2); SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"dodaj_odpowiedz_do_bazy\" as \"Fun\",\"Odpowiedź dodana do bazy\" as \"Info\"; else SELECT \"ERROR\" as \"Status\" , \"0\" as \"Bool\", \"dodaj_odpowiedz_do_bazy\" as \"Fun\",\"Odpowiedź nie została dodana do bazy\" as \"Info\"; end if; end",
                "CREATE PROCEDURE `dodaj_pytanie_do_bazy` (`vAnkietaNO` INT, `vTresc` VARCHAR(255)) begin SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo); if (@n1 is NOT NULL) then SET @n2 = (SELECT max(id_pytania) from pytania) + 1; INSERT INTO pytania(id_pytania, tresc) VALUES(@n2, vTresc); INSERT INTO ankiety_pytania(id_ankiety, id_pytania) VALUES(vAnkietaNO, @n2); SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"dodaj_pytanie_do_bazy\" as \"Fun\",\"Pytanie dodane do bazy\" as \"Info\"; else SELECT \"ERROR\" as \"Status\" , \"0\" as \"Bool\", \"dodaj_pytanie_do_bazy\" as \"Fun\",\"Pytanie nie zostało dodane do bazy\" as \"Info\"; end if; end",
                "CREATE PROCEDURE `pobierz_NIU` ()  begin INSERT INTO uzytkownicy(id_uzytkownika) VALUES(NULL); SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"pobierz_NIU\" as \"Fun\",\"User added correctly\" as \"Info\", LAST_INSERT_ID() as \"niu\"; end",
                "CREATE PROCEDURE `skasuj_ankiete` (`vAnkietaNO` INT) begin DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO); DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO); DELETE FROM pytania_odpowiedzi WHERE id_pytania in (Select id_pytania FROM ankiety_pytania ap WHERE ap.id_ankiety = vAnkietaNO); DELETE FROM pytania WHERE id_pytania IN (SELECT id_pytania FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO); DELETE FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO; DELETE FROM ankiety WHERE id_ankiety = vAnkietaNO; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"skasuj_ankietee\" as \"Fun\",\"Ankieta, pytania i odpowiedzi skasowane\" as \"Info\"; end",
                "CREATE PROCEDURE `skasuj_odpowiedz` (`vOdpowiedzNO` INT) begin DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi = vOdpowiedzNO; DELETE FROM pytania_odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO; DELETE FROM odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"skasuj_odpowiedz\" as \"Fun\",\"Odpowiedź skasowana\" as \"Info\"; end",
                "CREATE PROCEDURE `skasuj_pytanie` (`vPytanieNO` INT) begin DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO); DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO); DELETE FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO; DELETE FROM ankiety_pytania WHERE id_pytania = vPytanieNO; DELETE FROM pytania WHERE id_pytania = vPytanieNO; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"skasuj_pytanie\" as \"Fun\",\"Pytanie i odpowiedzi skasowane\" as \"Info\"; end",
                "CREATE PROCEDURE `toggle_ankieta` (`vAnkietaNO` INT)  begin SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo); if (@n1 is NOT NULL) then if( @n1 = 0) then UPDATE ankiety SET dostepnosc = 1 WHERE id_ankiety = vAnkietaNO; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"toggle_ankieta\" as \"Fun\",\"Status ankiety zmieniony\" as \"Info\"; else UPDATE ankiety SET dostepnosc = 0 WHERE id_ankiety = vAnkietaNO; SELECT \"DONE\" as \"Status\" , \"1\" as \"Bool\", \"toggle_ankieta\" as \"Fun\",\"Status ankiety zmieniony\" as \"Info\"; end if; else SELECT \"ERROR\" as \"Status\" , \"0\" as \"Bool\", \"toggle_ankieta\" as \"Fun\",\"Status ankiety nie został zmieniony\" as \"Info\"; end if; end",
                "CREATE TABLE `ankiety` (`id_ankiety` int(10) NOT NULL, `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL, `dostepnosc` int(1) NOT NULL DEFAULT '1') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `ankiety` (`id_ankiety`, `nazwa`, `dostepnosc`) VALUES (1, 'Ankieta Testowa v1', 0), (2, 'Ankieta Testowa v2 ze znakami ąśżźćółń', 0), (8, 'Ankieta numer 8 z 4 pytaniami', 1)",
                "CREATE TABLE `ankiety_pytania` (`id_ankiety` int(10) NOT NULL, `id_pytania` int(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `ankiety_pytania` (`id_ankiety`, `id_pytania`) VALUES(1, 1), (1, 2),(2, 3), (8, 4), (8, 5),(8, 6), (8, 7)",
                "CREATE TABLE `odpowiedzi` ( `id_odpowiedzi` int(10) NOT NULL, `odpowiedz` varchar(1024) COLLATE utf8_polish_ci NOT NULL, `ilosc_odpowiedzi` int(10) NOT NULL DEFAULT '0') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `odpowiedzi` (`id_odpowiedzi`, `odpowiedz`, `ilosc_odpowiedzi`) VALUES(1, 'Tak', 3),(2, 'Nie', 1), (3, 'Może', 2), (4, 'Treść jest zadowalająca', 0), (5, 'Treść nie jest zadowalająca', 3), (6, 'Treść może jest zadowalająca', 1),(7, 'Treść być może nie jest zadowalająca', 1), (8, 'To tylko testowe pytanie', 9), (9, 'Wiec wybierz losowa odpowiedź', 6), (10, 'ze wszystkich', 2), (11, '4 możliwych', 14), (12, 'Warto robić ankiety', 5), (13, 'Nie warto robić ankiet', 12), (14, 'Ankiety sa spoko', 12), (15, 'Procesory Intel sa najlepsze', 4), (16, 'Intel forever', 4),(17, 'Nie bo Meltdown', 8), (18, 'Nigdy nie były dobre', 2),(19, 'I tak lepsze od AMD', 9), (20, 'AMD zdecydwoanie lepsze', 8), (21, 'Niestety tylko Intel', 2), (22, 'AMD lepsze bo Meltdown', 7), (23, 'AMD tańszy', 8)",
                "CREATE TABLE `odpowiedzi_uzytkownicy` (`id_odpowiedzi` int(11) NOT NULL, `id_uzytkownika` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `odpowiedzi_uzytkownicy` (`id_odpowiedzi`, `id_uzytkownika`) VALUES(1, 11),(6, 11),(3, 13),(5, 13),(2, 14),(5, 14),(3, 16),(7, 16),(11, 20),(14, 20),(18, 20),(20, 20),(8, 21),(12, 21),(15, 21),(20, 21),(8, 22),(12, 22),(15, 22),(20, 22),(9, 23),(13, 23),(16, 23),(21, 23),(8, 24),(13, 24),(17, 24),(23, 24),(11, 25),(14, 25),(16, 25),(20, 25),(8, 26),(12, 26),(15, 26),(20, 26),(11, 27),(14, 27),(19, 27),(22, 27),(11, 59),(12, 59),(17, 59),(23, 59),(11, 60),(14, 60),(19, 60),(23, 60),(10, 61),(12, 61),(8, 62),(13, 62),(17, 62),(23, 62),(11, 62),(11, 63),(11, 63),(14, 63),(19, 63),(20, 63),(8, 63),(14, 63),(17, 63),(22, 63),(11, 64),(13, 64),(17, 64),(20, 64),(1, 64),(5, 64),(8, 72),(13, 72),(17, 72),(22, 72),(10, 73),(13, 73),(11, 74),(13, 74),(15, 74),(22, 74),(11, 75),(13, 75),(19, 75),(22, 75),(11, 76),(13, 76),(17, 76),(22, 76),(9, 77),(14, 77),(19, 77),(11, 78),(13, 78),(19, 78),(23, 78),(9, 80),(14, 80),(18, 80),(22, 80),(11, 81),(14, 81),(17, 81),(23, 81),(9, 82),(13, 82),(16, 82),(9, 83),(14, 83),(19, 83),(21, 83),(9, 84),(14, 84),(19, 84),(23, 84),(8, 86),(14, 86),(16, 86),(20, 86),(8, 87),(13, 87),(19, 87),(23, 87)",
                "CREATE TABLE `pytania` (`id_pytania` int(10) NOT NULL,`tresc` varchar(1024) COLLATE utf8_polish_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `pytania` (`id_pytania`, `tresc`) VALUES(1, 'Czy ta ankieta jest spoko?'),(2, 'Czy treść jest zadowalająca'),(3, 'Pytanie bez odpowiedzi'), (4, 'Pytanie testowe nr1'), (5, 'Czy warto robić ankiety?'), (6, 'Czy procesory Intel sa dobre?'), (7, 'Czy procesory AMD sa lepsze?')",
                "CREATE TABLE `pytania_odpowiedzi` (`id_pytania` int(10) NOT NULL,`id_odpowiedzi` int(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `pytania_odpowiedzi` (`id_pytania`, `id_odpowiedzi`) VALUES(1, 1),(1, 2),(1, 3),(2, 4),(2, 5),(2, 6),(2, 7),(4, 8),(4, 9),(4, 10),(4, 11),(5, 12),(5, 13),(5, 14),(6, 15),(6, 16),(6, 17),(6, 18),(6, 19),(7, 20),(7, 21),(7, 22),(7, 23)",
                "CREATE TABLE `uzytkownicy` (`id_uzytkownika` int(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci",
                "INSERT INTO `uzytkownicy` (`id_uzytkownika`) VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58),(59),(60),(61),(62),(63),(64),(65),(66),(67),(68),(69),(70),(71),(72),(73),(74),(75),(76),(77),(78),(79),(80),(81),(82),(83),(84),(85),(86),(87)",
                "ALTER TABLE `ankiety` ADD PRIMARY KEY (`id_ankiety`)",
                "ALTER TABLE `ankiety_pytania` ADD KEY `id_ankiety` (`id_ankiety`), ADD KEY `id_pytania` (`id_pytania`)",
                "ALTER TABLE `odpowiedzi` ADD PRIMARY KEY (`id_odpowiedzi`)",
                "ALTER TABLE `odpowiedzi_uzytkownicy` ADD KEY `id_odpowiedzi` (`id_odpowiedzi`), ADD KEY `id_uzytkownika` (`id_uzytkownika`)",
                "ALTER TABLE `pytania` ADD PRIMARY KEY (`id_pytania`)",
                "ALTER TABLE `pytania_odpowiedzi` ADD KEY `id_pytania` (`id_pytania`), ADD KEY `id_odpowiedzi` (`id_odpowiedzi`)",
                "ALTER TABLE `uzytkownicy` ADD PRIMARY KEY (`id_uzytkownika`)",
                "ALTER TABLE `ankiety` MODIFY `id_ankiety` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9",
                "ALTER TABLE `odpowiedzi` MODIFY `id_odpowiedzi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24",
                "ALTER TABLE `pytania` MODIFY `id_pytania` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8",
                "ALTER TABLE `uzytkownicy` MODIFY `id_uzytkownika` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88",
                "ALTER TABLE `ankiety_pytania` ADD CONSTRAINT `ankiety_pytania_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `ankiety_pytania_ibfk_2` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE",
                "ALTER TABLE `odpowiedzi_uzytkownicy` ADD CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_1` FOREIGN KEY (`id_uzytkownika`) REFERENCES `uzytkownicy` (`id_uzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE",
                "ALTER TABLE `pytania_odpowiedzi` ADD CONSTRAINT `pytania_odpowiedzi_ibfk_1` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE, ADD CONSTRAINT `pytania_odpowiedzi_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE"
        };

        Statement stmt = null;
        try {
            dbConnect();
            stmt = conn.createStatement();
            for(int i=0; i< DBSuperString.length ; i++)
                stmt.executeUpdate(DBSuperString[i]);
        } catch (SQLException e) {
            System.err.println("Error while creating Tables");
            e.printStackTrace();
            return false;
        } finally {
            if(stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            try {
                dbDisconnect();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static boolean checkIfDBExist(String dbName){
        try{
            DBUtil.mysqlConnect();
            ResultSet resultSet = conn.getMetaData().getCatalogs();
            while (resultSet.next()) {
                String databaseName = resultSet.getString(1);
                if(databaseName.equals(dbName)){
                    return true;
                }
            }
            resultSet.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                DBUtil.dbDisconnect();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }
}
