-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: ankiety_wadolny
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ankiety_wadolny`
--

/*!40000 DROP DATABASE IF EXISTS `ankiety_wadolny`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ankiety_wadolny` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ankiety_wadolny`;

--
-- Table structure for table `ankiety`
--

DROP TABLE IF EXISTS `ankiety`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ankiety` (
  `id_ankiety` int(10) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `dostepnosc` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_ankiety`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ankiety`
--

LOCK TABLES `ankiety` WRITE;
/*!40000 ALTER TABLE `ankiety` DISABLE KEYS */;
INSERT INTO `ankiety` VALUES (1,'Ankieta Testowa v1',1),(2,'Ankieta Testowa v2 ze znakami ąśżźćółń',1),(8,'Ankieta numer 3 z 3 pytaniami',1);
/*!40000 ALTER TABLE `ankiety` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ankiety_pytania`
--

DROP TABLE IF EXISTS `ankiety_pytania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ankiety_pytania` (
  `id_ankiety` int(10) NOT NULL,
  `id_pytania` int(10) NOT NULL,
  KEY `id_ankiety` (`id_ankiety`),
  KEY `id_pytania` (`id_pytania`),
  CONSTRAINT `ankiety_pytania_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ankiety_pytania_ibfk_2` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ankiety_pytania`
--

LOCK TABLES `ankiety_pytania` WRITE;
/*!40000 ALTER TABLE `ankiety_pytania` DISABLE KEYS */;
INSERT INTO `ankiety_pytania` VALUES (1,1),(1,2),(2,3),(8,4),(8,5),(8,6),(8,7);
/*!40000 ALTER TABLE `ankiety_pytania` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odpowiedzi`
--

DROP TABLE IF EXISTS `odpowiedzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odpowiedzi` (
  `id_odpowiedzi` int(10) NOT NULL AUTO_INCREMENT,
  `odpowiedz` varchar(1024) COLLATE utf8_polish_ci NOT NULL,
  `ilosc_odpowiedzi` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_odpowiedzi`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odpowiedzi`
--

LOCK TABLES `odpowiedzi` WRITE;
/*!40000 ALTER TABLE `odpowiedzi` DISABLE KEYS */;
INSERT INTO `odpowiedzi` VALUES (1,'Tak',3),(2,'Nie',1),(3,'Może',2),(4,'Treść jest zadowalająca',0),(5,'Treść nie jest zadowalająca',3),(6,'Treść może jest zadowalająca',1),(7,'Treść być może nie jest zadowalająca',1),(8,'To tylko testowe pytanie',8),(9,'Wiec wybierz losowa odpowiedz',6),(10,'ze wszystkich',2),(11,'4 mozliwych',14),(12,'Warto robic ankiety',5),(13,'Nie warto robic ankiet',11),(14,'Ankiety sa spoko',12),(15,'Procesory Intel sa najlepsze',4),(16,'Intel forever',4),(17,'Nie bo Meltdown',8),(18,'Nigdy nie byly dobre',2),(19,'I tak lepsze od AMD',8),(20,'AMD zdecydwoanie lepsze',8),(21,'Niestety tylko Intel',2),(22,'AMD lepsze bo Meltdown',7),(23,'AMD tanszy',7);
/*!40000 ALTER TABLE `odpowiedzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odpowiedzi_uzytkownicy`
--

DROP TABLE IF EXISTS `odpowiedzi_uzytkownicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odpowiedzi_uzytkownicy` (
  `id_odpowiedzi` int(11) NOT NULL,
  `id_uzytkownika` int(11) NOT NULL,
  KEY `id_odpowiedzi` (`id_odpowiedzi`),
  KEY `id_uzytkownika` (`id_uzytkownika`),
  CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_1` FOREIGN KEY (`id_uzytkownika`) REFERENCES `uzytkownicy` (`id_uzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odpowiedzi_uzytkownicy`
--

LOCK TABLES `odpowiedzi_uzytkownicy` WRITE;
/*!40000 ALTER TABLE `odpowiedzi_uzytkownicy` DISABLE KEYS */;
INSERT INTO `odpowiedzi_uzytkownicy` VALUES (1,11),(6,11),(3,13),(5,13),(2,14),(5,14),(3,16),(7,16),(11,20),(14,20),(18,20),(20,20),(8,21),(12,21),(15,21),(20,21),(8,22),(12,22),(15,22),(20,22),(9,23),(13,23),(16,23),(21,23),(8,24),(13,24),(17,24),(23,24),(11,25),(14,25),(16,25),(20,25),(8,26),(12,26),(15,26),(20,26),(11,27),(14,27),(19,27),(22,27),(11,59),(12,59),(17,59),(23,59),(11,60),(14,60),(19,60),(23,60),(10,61),(12,61),(8,62),(13,62),(17,62),(23,62),(11,62),(11,63),(11,63),(14,63),(19,63),(20,63),(8,63),(14,63),(17,63),(22,63),(11,64),(13,64),(17,64),(20,64),(1,64),(5,64),(8,72),(13,72),(17,72),(22,72),(10,73),(13,73),(11,74),(13,74),(15,74),(22,74),(11,75),(13,75),(19,75),(22,75),(11,76),(13,76),(17,76),(22,76),(9,77),(14,77),(19,77),(11,78),(13,78),(19,78),(23,78),(9,80),(14,80),(18,80),(22,80),(11,81),(14,81),(17,81),(23,81),(9,82),(13,82),(16,82),(9,83),(14,83),(19,83),(21,83),(9,84),(14,84),(19,84),(23,84),(8,86),(14,86),(16,86),(20,86);
/*!40000 ALTER TABLE `odpowiedzi_uzytkownicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pytania`
--

DROP TABLE IF EXISTS `pytania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pytania` (
  `id_pytania` int(10) NOT NULL AUTO_INCREMENT,
  `tresc` varchar(1024) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_pytania`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pytania`
--

LOCK TABLES `pytania` WRITE;
/*!40000 ALTER TABLE `pytania` DISABLE KEYS */;
INSERT INTO `pytania` VALUES (1,'Czy ta ankieta jest spoko?'),(2,'Czy treść jest zadowalająca'),(3,'Pytanie bez odpowiedzi'),(4,'Pytanie testowe nr1'),(5,'Czy warto robic ankiety?'),(6,'Czy procesory Intel sa dobre?'),(7,'Czy procesory AMD sa lepsze?');
/*!40000 ALTER TABLE `pytania` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pytania_odpowiedzi`
--

DROP TABLE IF EXISTS `pytania_odpowiedzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pytania_odpowiedzi` (
  `id_pytania` int(10) NOT NULL,
  `id_odpowiedzi` int(10) NOT NULL,
  KEY `id_pytania` (`id_pytania`),
  KEY `id_odpowiedzi` (`id_odpowiedzi`),
  CONSTRAINT `pytania_odpowiedzi_ibfk_1` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pytania_odpowiedzi_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pytania_odpowiedzi`
--

LOCK TABLES `pytania_odpowiedzi` WRITE;
/*!40000 ALTER TABLE `pytania_odpowiedzi` DISABLE KEYS */;
INSERT INTO `pytania_odpowiedzi` VALUES (1,1),(1,2),(1,3),(2,4),(2,5),(2,6),(2,7),(4,8),(4,9),(4,10),(4,11),(5,12),(5,13),(5,14),(6,15),(6,16),(6,17),(6,18),(6,19),(7,20),(7,21),(7,22),(7,23);
/*!40000 ALTER TABLE `pytania_odpowiedzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uzytkownicy`
--

DROP TABLE IF EXISTS `uzytkownicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy` (
  `id_uzytkownika` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_uzytkownika`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uzytkownicy`
--

LOCK TABLES `uzytkownicy` WRITE;
/*!40000 ALTER TABLE `uzytkownicy` DISABLE KEYS */;
INSERT INTO `uzytkownicy` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58),(59),(60),(61),(62),(63),(64),(65),(66),(67),(68),(69),(70),(71),(72),(73),(74),(75),(76),(77),(78),(79),(80),(81),(82),(83),(84),(85),(86);
/*!40000 ALTER TABLE `uzytkownicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ankiety_wadolny'
--
/*!50003 DROP PROCEDURE IF EXISTS `dodaj_ankiete_do_bazy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_ankiete_do_bazy`(`vNazwa` VARCHAR(255))
begin
INSERT INTO ankiety(id_ankiety, nazwa) VALUES(NULL, vNazwa);
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_ankiete_do_bazy" as "Fun","Ankieta dodana do bazy" as "Info";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dodaj_odpowiedz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_odpowiedz`(`vNIU` INT, `vOdpowiedz` INT)
begin
INSERT INTO odpowiedzi_uzytkownicy(id_odpowiedzi, id_uzytkownika) VALUES(vOdpowiedz, vNIU);
UPDATE odpowiedzi SET ilosc_odpowiedzi = ilosc_odpowiedzi + 1 WHERE id_odpowiedzi = vOdpowiedz;
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz" as "Fun","Odpowiedź dodana do bazy" as "Info";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dodaj_odpowiedz_do_bazy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_odpowiedz_do_bazy`(`vPytanieNO` INT, `vTresc` VARCHAR(255))
begin
SET @n1 = (SELECT tresc from pytania where id_pytania = vPytanieNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_odpowiedzi) from odpowiedzi) + 1;
	INSERT INTO odpowiedzi(id_odpowiedzi, odpowiedz, ilosc_odpowiedzi) VALUES(@n2, vTresc, 0);
	INSERT INTO pytania_odpowiedzi(id_pytania, id_odpowiedzi) VALUES(vPytanieNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź dodana do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź nie została dodana do bazy" as "Info";
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dodaj_pytanie_do_bazy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_pytanie_do_bazy`(`vAnkietaNO` INT, `vTresc` VARCHAR(255))
begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_pytania) from pytania) + 1;
	INSERT INTO pytania(id_pytania, tresc) VALUES(@n2, vTresc);
	INSERT INTO ankiety_pytania(id_ankiety, id_pytania) VALUES(vAnkietaNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie dodane do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie nie zostało dodane do bazy" as "Info";
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pobierz_NIU` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pobierz_NIU`()
begin
INSERT INTO uzytkownicy(id_uzytkownika) VALUES(NULL);
SELECT "DONE" as "Status" , "1" as "Bool", "pobierz_NIU" as "Fun","User added correctly" as "Info", LAST_INSERT_ID() as "niu";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `skasuj_ankiete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_ankiete`(vAnkietaNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania in (Select id_pytania FROM ankiety_pytania ap WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania WHERE id_pytania IN (SELECT id_pytania FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO);
DELETE FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO;
DELETE FROM ankiety WHERE id_ankiety = vAnkietaNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_ankietee" as "Fun","Ankieta, pytania i odpowiedzi skasowane" as "Info";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `skasuj_odpowiedz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_odpowiedz`(vOdpowiedzNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM pytania_odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_odpowiedz" as "Fun","Odpowiedź skasowana" as "Info";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `skasuj_pytanie` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_pytanie`(vPytanieNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO;
DELETE FROM ankiety_pytania WHERE id_pytania = vPytanieNO;
DELETE FROM pytania WHERE id_pytania = vPytanieNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_pytanie" as "Fun","Pytanie i odpowiedzi skasowane" as "Info";
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `toggle_ankieta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `toggle_ankieta`(`vAnkietaNO` INT)
begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	if( @n1 = 0) then
		UPDATE ankiety SET dostepnosc = 1 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	else
		UPDATE ankiety SET dostepnosc = 0 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	end if;
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "toggle_ankieta" as "Fun","Status ankiety nie został zmieniony" as "Info";
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-16 19:24:21
