﻿#=========================================================================================================
#=========================================== TABELE (TEMPORARY) ===================================================
#=========================================================================================================
CREATE TABLE `CarPark`.`Ticket` ( `TicketNo` INT NOT NULL AUTO_INCREMENT , `EntryTime` DATE NOT NULL , `LeaveTime` DATE NULL DEFAULT NULL , `PaymentTime` DATE NULL DEFAULT NULL , `PaymentType` ENUM('cash','subscription') NULL DEFAULT NULL , `Charge` INT NULL DEFAULT NULL , PRIMARY KEY (`TicketNo`)) ENGINE = InnoDB;

CREATE TABLE `carpark`.`customer` ( `CustNo` INT NOT NULL AUTO_INCREMENT , `Name` VARCHAR(50) NOT NULL , `Surname` VARCHAR(50) NOT NULL , `Login` VARCHAR(50) NOT NULL , `Password` VARCHAR(50) NOT NULL , `Phone` INT(9) NULL , `Email` VARCHAR(50) NULL , PRIMARY KEY (`CustNo`)) ENGINE = InnoDB;

CREATE TABLE `carpark`.`Subscription` ( `SubNo` INT NOT NULL AUTO_INCREMENT , `StartTime` DATE NOT NULL , `EndTime` DATE NOT NULL , `PurchaseTime` DATE NOT NULL , `Typ` ENUM('30days','90days','180days','1year','unlimited') NOT NULL , PRIMARY KEY (`SubNo`)) ENGINE = InnoDB;


ALTER TABLE `user_ticket` ADD CONSTRAINT `user_ticket_fk_1` FOREIGN KEY (`UserNo`) REFERENCES `user` (`UserNo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `user_ticket` ADD CONSTRAINT `user_ticket_fk_2` FOREIGN KEY (`TicketNo`) REFERENCES `ticket` (`TicketNo`);

ALTER TABLE `user_sub` ADD CONSTRAINT `user_sub_fk_1` FOREIGN KEY (`UserNo`) REFERENCES `user` (`UserNo`);

ALTER TABLE `user_sub` ADD CONSTRAINT `user_sub_fk_2` FOREIGN KEY (`SubNo`) REFERENCES `subscription` (`SubNo`);


#=========================================================================================================
#=========================================== PROCEDURY ===================================================
#=========================================================================================================

#=========================================================================================================
# procedura dodająca nowego usera       		 ========= pobierz_NIU
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE pobierz_NIU()
begin
INSERT INTO uzytkownicy(id_uzytkownika) VALUES(NULL);
SELECT "DONE" as "Status" , "1" as "Bool", "pobierz_NIU" as "Fun","User added correctly" as "Info", LAST_INSERT_ID() as "niu";
end
//
DELIMITER ;

#====================
CALL pobierz_NIU();
#====================

#=========================================================================================================
# procedura dodająca nową odpowiedź użytkownika- odpowedź na pytanie    ========= dodaj_odpowiedz
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE dodaj_odpowiedz(vNIU int, vOdpowiedz int)
begin
INSERT INTO odpowiedzi_uzytkownicy(id_odpowiedzi, id_uzytkownika) VALUES(vOdpowiedz, vNIU);
UPDATE odpowiedzi SET ilosc_odpowiedzi = ilosc_odpowiedzi + 1 WHERE id_odpowiedzi = vOdpowiedz;
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz" as "Fun","Odpowiedź dodana do bazy" as "Info";
end
//
DELIMITER ;

#====================
CALL dodaj_odpowiedz(userNIU, clientAnswer);
#====================

#=========================================================================================================
# procedura dodająca nową ankietę do bazy     		 ========= dodaj_ankiete_do_bazy
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE dodaj_ankiete_do_bazy(vNazwa varchar(255))
begin
INSERT INTO ankiety(id_ankiety, nazwa) VALUES(NULL, vNazwa);
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_ankiete_do_bazy" as "Fun","Ankieta dodana do bazy" as "Info";
end
//
DELIMITER ;

#====================
CALL dodaj_ankiete_do_bazy(vNazwa);
#====================

#=========================================================================================================
# procedura zmieniajaca status ankiety na włączony/wyłączony     		 ========= toggle_ankieta
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE toggle_ankieta(vAnkietaNO int)
begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	if( @n1 = 0) then
		UPDATE ankiety SET dostepnosc = 1 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	else
		UPDATE ankiety SET dostepnosc = 0 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	end if;
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "toggle_ankieta" as "Fun","Status ankiety nie został zmieniony" as "Info";
end if;
end
//
DELIMITER ;

#====================
CALL toggle_ankieta(vAnkietaNO);
#====================

#=========================================================================================================
# procedura dodająca nowe pytanie do bazy     		 ========= dodaj_pytanie_do_bazy 
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE dodaj_pytanie_do_bazy(vAnkietaNO int, vTresc varchar(255))
begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_pytania) from pytania) + 1;
	INSERT INTO pytania(id_pytania, tresc) VALUES(@n2, vTresc);
	INSERT INTO ankiety_pytania(id_ankiety, id_pytania) VALUES(vAnkietaNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie dodane do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie nie zostało dodane do bazy" as "Info";
end if;
end
//
DELIMITER ;

#====================
CALL dodaj_pytanie_do_bazy(vAnkietaNo, vTresc);
#====================

#=========================================================================================================
# procedura dodająca nową odpowiedz do bazy    		 ========= dodaj_odpowiedz_do_bazy
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE dodaj_odpowiedz_do_bazy(vPytanieNO int, vTresc varchar(255))
begin
SET @n1 = (SELECT tresc from pytania where id_pytania = vPytanieNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_odpowiedzi) from odpowiedzi) + 1;
	INSERT INTO odpowiedzi(id_odpowiedzi, odpowiedz, ilosc_odpowiedzi) VALUES(@n2, vTresc, 0);
	INSERT INTO pytania_odpowiedzi(id_pytania, id_odpowiedzi) VALUES(vPytanieNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź dodana do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź nie została dodana do bazy" as "Info";
end if;
end
//
DELIMITER ;

#====================
CALL dodaj_odpowiedz_do_bazy(vPytanieNo, vTresc);
#====================

#=========================================================================================================
# procedura kasująca ankietę i wszystkie odpowiedzi z niej / statystyki ========= skasuj_ankiete
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE skasuj_ankiete(vAnkietaNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania in (Select id_pytania FROM ankiety_pytania ap WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania WHERE id_pytania IN (SELECT id_pytania FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO);
DELETE FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO;
DELETE FROM ankiety WHERE id_ankiety = vAnkietaNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_ankietee" as "Fun","Ankieta, pytania i odpowiedzi skasowane" as "Info";
end
//
DELIMITER ;

#====================
CALL skasuj_ankiete(vAnkietaNO);
#====================

#=========================================================================================================
# procedura kasująca pytanie i wszystkie odpowiedzi z niej / statystyki ========= skasuj_pytanie
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE skasuj_pytanie(vPytanieNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO;
DELETE FROM ankiety_pytania WHERE id_pytania = vPytanieNO;
DELETE FROM pytania WHERE id_pytania = vPytanieNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_pytanie" as "Fun","Pytanie i odpowiedzi skasowane" as "Info";
end
//
DELIMITER ;
#====================
CALL skasuj_pytanie(vPytanieNO);
#====================

#=========================================================================================================
# procedura kasująca odpowiedz z pytania i wszystkie statystyki tej odpowiedzi ========= skasuj_odpowiedz
#=========================================================================================================
DELIMITER //
CREATE PROCEDURE skasuj_odpowiedz(vOdpowiedzNO int)
begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM pytania_odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_odpowiedz" as "Fun","Odpowiedź skasowana" as "Info";
end
//
DELIMITER ;

#====================
CALL skasuj_odpowiedz(vOdpowiedzNO);
#====================